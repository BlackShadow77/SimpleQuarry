package org.zeith.simplequarry;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.intent.IntentManager;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.hammercore.internal.variables.VariableManager;
import com.zeitheron.hammercore.internal.variables.types.VariableCompoundNBT;
import com.zeitheron.hammercore.utils.HammerCoreUtils;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zeith.simplequarry.cfg.ConfigsSQ;
import org.zeith.simplequarry.init.BlocksSQ;
import org.zeith.simplequarry.init.GuisSQ;
import org.zeith.simplequarry.init.ItemsSQ;
import org.zeith.simplequarry.proxy.CommonProxy;

import java.util.HashSet;
import java.util.Set;

@Mod(modid = InfoSQ.MOD_ID, name = InfoSQ.MOD_NAME, version = InfoSQ.MOD_VERSION, dependencies = "required-after:hammercore", guiFactory = "org.zeith.simplequarry.cfg.ConfigFactorySQ", certificateFingerprint = HammerCore.CERTIFICATE_FINGERPRINT, updateJSON = "https://dccg.herokuapp.com/api/fmluc/247393")
public class SimpleQuarry
{
	@SidedProxy(modId = InfoSQ.MOD_ID, clientSide = "org.zeith.simplequarry.proxy.ClientProxy", serverSide = "org.zeith.simplequarry.proxy.CommonProxy")
	public static CommonProxy proxy;

	@Mod.Instance(InfoSQ.MOD_ID)
	public static SimpleQuarry instance;

	public static boolean invalidCertificate;
	public static final VariableCompoundNBT SYNC_CONFIGS = new VariableCompoundNBT(new ResourceLocation(InfoSQ.MOD_ID, "configs"));
	public static CreativeTabs tab;
	public static final Set<IBlockState> QUARRY_BLACKLIST = new HashSet<IBlockState>();
	public static final Logger LOG = LogManager.getLogger(InfoSQ.MOD_ID);

	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with SimpleQuarry jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://minecraft.curseforge.com/projects/247393 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put(InfoSQ.MOD_ID, "https://minecraft.curseforge.com/projects/247393");
		invalidCertificate = true;
	}

	@EventHandler
	public void construct(FMLConstructionEvent e)
	{
		IntentManager.registerIntentHandler("simplequarry:quarry_blacklist", IBlockState.class, (mod, data) -> QUARRY_BLACKLIST.add(data));
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		VariableManager.registerVariable(SYNC_CONFIGS);

		SYNC_CONFIGS.set(new NBTTagCompound());

		tab = HammerCoreUtils.createDynamicCreativeTab("simplequarry", 80);

		MinecraftForge.EVENT_BUS.register(proxy);
		MinecraftForge.EVENT_BUS.register(this);

		proxy.preInit();

		SimpleRegistration.registerFieldBlocksFrom(BlocksSQ.class, "simplequarry", tab);
		SimpleRegistration.registerFieldItemsFrom(ItemsSQ.class, "simplequarry", tab);

		MinecraftForge.EVENT_BUS.post(new ConfigChangedEvent("simplequarry", null, false, false));
	}

	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
		new GuisSQ();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e)
	{
		proxy.postInit();
	}

	@EventHandler
	public void serverStarting(FMLServerStartingEvent e)
	{
		proxy.serverStarting();
		ConfigsSQ.updateVariable();
	}

	@EventHandler
	public void serverStarted(FMLServerStartedEvent e)
	{
		proxy.serverStarted();
	}

	@EventHandler
	public void serverStopping(FMLServerStoppingEvent e)
	{
		proxy.serverStopping();
		SYNC_CONFIGS.set(new NBTTagCompound());
	}

	@EventHandler
	public void serverStopped(FMLServerStoppedEvent e)
	{
		proxy.serverStopped();
	}
}
