package org.zeith.simplequarry.items;

import com.zeitheron.hammercore.utils.IRegisterListener;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import org.zeith.simplequarry.tile.TilePoweredQuarry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ItemUpgrade
		extends Item
		implements IRegisterListener
{
	private static final List<ItemUpgrade> UPGRADE_LIST = new ArrayList<>();
	public static final List<ItemUpgrade> ALL_UPGRADES = Collections.unmodifiableList(UPGRADE_LIST);

	protected float quarryUseMultiplier = 1;

	public float quarryUseMultiplierServer = 1;
	public float quarryUseMultiplierClient = 1;

	{
		setMaxStackSize(1);
	}

	public float getBaseQuarryUseMultiplier()
	{
		return quarryUseMultiplier;
	}

	@Override
	public void onRegistered()
	{
		UPGRADE_LIST.add(this);
		quarryUseMultiplierClient = quarryUseMultiplierServer = quarryUseMultiplier;
	}

	public void handleDrops(TilePoweredQuarry quarry, BlockPos pos, NonNullList<ItemStack> drops)
	{

	}

	public boolean canStay(TilePoweredQuarry quarry, int index)
	{
		return true;
	}

	public ItemStack handlePickup(ItemStack stack, TilePoweredQuarry quarry, int index)
	{
		return stack;
	}

	public boolean isCompatible(TilePoweredQuarry quarry)
	{
		return true;
	}

	public void tick(TilePoweredQuarry quarry, int index)
	{
	}

	public static boolean hasUpgrade(TilePoweredQuarry quarry, ItemUpgrade upgrade)
	{
		for(ItemUpgrade up : quarry.getUpgrades())
			if(up == upgrade)
				return true;
		return false;
	}
}