package org.zeith.simplequarry.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.network.Packet;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import org.zeith.simplequarry.api.energy.UniversalConverter;
import org.zeith.simplequarry.cfg.ConfigsSQ;
import org.zeith.simplequarry.gui.c.ContainerFuelQuarry;
import org.zeith.simplequarry.gui.c.ContainerPoweredQuarry;
import org.zeith.simplequarry.items.ItemUpgrade;
import org.zeith.simplequarry.tile.TileFuelQuarry;
import org.zeith.simplequarry.vortex.Vortex;

import java.util.HashSet;
import java.util.Set;

public class ClientProxy
		extends CommonProxy
{
	public static Set<Vortex> particleVortex = new HashSet<>();

	@Override
	public void addParticleVortex(Vortex vortex)
	{
		if(vortex == null || vortex.getVortexStrength() == 0.0 || particleVortex.contains(vortex))
			return;
		HashSet<Vortex> particleVortex = new HashSet<>(ClientProxy.particleVortex);
		particleVortex.add(vortex);
		ClientProxy.particleVortex = particleVortex;
	}

	@Override
	public void removeParticleVortex(Vortex vortex)
	{
		if(vortex == null || vortex.getVortexStrength() == 0.0 || !particleVortex.contains(vortex))
			return;
		HashSet<Vortex> particleVortex = new HashSet<>(ClientProxy.particleVortex);
		particleVortex.remove(vortex);
		ClientProxy.particleVortex = particleVortex;
	}

	@SubscribeEvent
	public void tickParticle(TickEvent.ClientTickEvent evt)
	{
		if(evt.phase == TickEvent.Phase.START)
		{
			WorldClient world = Minecraft.getMinecraft().world;
			int dim = Integer.MAX_VALUE;
			if(world == null)
				particleVortex.clear();
			else
				dim = world.provider.getDimension();

			for(Vortex vortex : particleVortex)
				if(dim == vortex.world)
					vortex.update();
				else
					removeParticleVortex(vortex);
		}
	}

	@Override
	public void sendPacket(Packet<?> pkt)
	{
		Minecraft.getMinecraft().player.connection.sendPacket(pkt);
	}

	@SubscribeEvent
	public void tooltip(ItemTooltipEvent e)
	{
		EntityPlayer p = e.getEntityPlayer();
		if(p == null)
			return;

		ItemStack it = e.getItemStack();

		if(!it.isEmpty() && it.getItem() instanceof ItemUpgrade)
		{
			ItemUpgrade up = (ItemUpgrade) it.getItem();

			e.getToolTip().add(TextFormatting.DARK_PURPLE + I18n.format("info.simplequarry:fuel_use_boost", Math.round(up.quarryUseMultiplierClient * 1000F) / 1000F));
		}

		if(p.openContainer instanceof ContainerFuelQuarry || p.openContainer instanceof ContainerPoweredQuarry)
		{
			TileFuelQuarry quarry = null;

			if(p.openContainer instanceof ContainerFuelQuarry)
				quarry = ((ContainerFuelQuarry) p.openContainer).t;

			if(p.openContainer instanceof ContainerPoweredQuarry)
				quarry = ((ContainerPoweredQuarry) p.openContainer).t;

			int burnTime = TileEntityFurnace.getItemBurnTime(e.getItemStack());

			if(burnTime > 0)
			{
				float mod = quarry != null ? quarry.getUsageMult() : 1F;
				e.getToolTip().add(TextFormatting.DARK_GRAY + I18n.format("info.simplequarry:blocks_broken") + ": " + (int) (UniversalConverter.FT_QF(burnTime / mod) / (UniversalConverter.FT_QF(TileEntityFurnace.getItemBurnTime(COAL)) / ConfigsSQ.getBlockPerCoal())));
				e.getToolTip().add(TextFormatting.DARK_GRAY + I18n.format("info.simplequarry:fuel_use_boost") + ": " + (int) (mod * 100) + "%");
			} else
				e.getToolTip().add(TextFormatting.DARK_GRAY + I18n.format("info.simplequarry:not_fuel"));
		}
	}
}
