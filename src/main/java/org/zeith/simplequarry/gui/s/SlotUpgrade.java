package org.zeith.simplequarry.gui.s;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import org.zeith.simplequarry.items.ItemUpgrade;
import org.zeith.simplequarry.tile.TilePoweredQuarry;

public class SlotUpgrade extends Slot
{
	final TilePoweredQuarry quarry;
	
	public SlotUpgrade(IInventory inventoryIn, int index, int xPosition, int yPosition, TilePoweredQuarry quarry)
	{
		super(inventoryIn, index, xPosition, yPosition);
		this.quarry = quarry;
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		if(!stack.isEmpty() && stack.getItem() instanceof ItemUpgrade)
			return ((ItemUpgrade) stack.getItem()).isCompatible(quarry);
		return false;
	}
}