package org.zeith.simplequarry.init;

import net.minecraft.block.Block;
import org.zeith.simplequarry.blocks.BlockFuelQuarry;
import org.zeith.simplequarry.blocks.BlockPoweredQuarry;
import org.zeith.simplequarry.cfg.ConfigsSQ;

public class BlocksSQ
{
	public static final Block FUEL_QUARRY = new BlockFuelQuarry();
	public static final Block POWERED_QUARRY = ConfigsSQ.getQuarryRecipe() == -1 ? null : new BlockPoweredQuarry();
}