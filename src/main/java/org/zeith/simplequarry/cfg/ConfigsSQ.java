package org.zeith.simplequarry.cfg;

import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyFloat;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyInt;
import com.zeitheron.hammercore.internal.variables.IVariable;
import com.zeitheron.hammercore.internal.variables.VariableRefreshEvent;
import com.zeitheron.hammercore.net.internal.PacketUpdateDirtyVariables;
import com.zeitheron.hammercore.net.transport.NetTransport;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.zeith.simplequarry.InfoSQ;
import org.zeith.simplequarry.SimpleQuarry;
import org.zeith.simplequarry.items.ItemUpgrade;

import java.util.Collections;
import java.util.OptionalDouble;
import java.util.OptionalInt;

@Mod.EventBusSubscriber
@HCModConfigurations(modid = InfoSQ.MOD_ID)
public class ConfigsSQ
		implements IConfigReloadListener
{
	@ModConfigPropertyInt(name = "Powered Quarry Recipe", category = "Gameplay", defaultValue = 1, min = -1, max = 1, comment = "What recipe should be used for Powered Quarry?\n-1 - disable quarry at all\n0 - 4 eyes of ender & normal chest\n1 - podzol, sea lantern, slime block, magma block & ender chest")
	private static int POWERED_QUARRY_RECIPE;

	@ModConfigPropertyFloat(name = "Blocks Per Coal", category = "Gameplay", defaultValue = 96F, min = 1F, max = 65536F, comment = "How much blocks can 1 coal mine? This value is taken for all other fuel types as a standard.")
	private static float BLOCKS_PER_COAL;

	@ModConfigPropertyInt(name = "Fuel Quarry Mine Tick Rate", category = "Gameplay", defaultValue = 10, min = 1, max = 65536, comment = "How frequently the Fuel Quarry will mine blocks?")
	public static int FUEL_QUARRY_TICK_RATE;

	@ModConfigPropertyInt(name = "Powered Quarry Mine Tick Rate", category = "Gameplay", defaultValue = 5, min = 1, max = 65536, comment = "How frequently the Powered Quarry will mine blocks?")
	public static int POWERED_QUARRY_TICK_RATE;

	public static Configuration cfgs;

	@Override
	public void reloadCustom(Configuration cfgs)
	{
		ConfigsSQ.cfgs = cfgs;

		ItemUpgrade.ALL_UPGRADES
				.forEach(u ->
						u.quarryUseMultiplierServer = cfgs.getFloat(u.getRegistryName().toString(), "Upgrade Usage Rates", u.getBaseQuarryUseMultiplier(), 0F, 1000F, "Fuel rate for " + u.getRegistryName().getPath())
				);

		Side effSide = FMLCommonHandler.instance().getEffectiveSide();

		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();

		if(server != null && (effSide.isServer() || (effSide.isClient() && server instanceof IntegratedServer)))
		{
			server.addScheduledTask(() ->
			{
				updateVariable();
				NetTransport.wrap(new PacketUpdateDirtyVariables(Collections.singletonMap(SimpleQuarry.SYNC_CONFIGS.getId(), SimpleQuarry.SYNC_CONFIGS))).sendToAll();
			});
		}
	}

	private static OptionalDouble bpcOverride = OptionalDouble.empty();
	private static OptionalInt pqrOverride = OptionalInt.empty();

	public static void updateVariable()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setFloat("BPC", ConfigsSQ.BLOCKS_PER_COAL);
		nbt.setInteger("QuarryRecipe", ConfigsSQ.POWERED_QUARRY_RECIPE);

		NBTTagCompound lst = new NBTTagCompound();
		ItemUpgrade.ALL_UPGRADES
				.forEach(u ->
				{
					NBTTagCompound comp = new NBTTagCompound();
					comp.setFloat("Mult", u.quarryUseMultiplierServer);
					lst.setTag(u.getRegistryName().toString(), comp);
				});
		nbt.setTag("Upgrades", lst);

		SimpleQuarry.SYNC_CONFIGS.set(nbt);

		SimpleQuarry.LOG.info("Updated server configs: " + nbt);
	}

	@SubscribeEvent
	public static void updateVar(VariableRefreshEvent evt)
	{
		IVariable<?> var = evt.getVariable();
		if(var == SimpleQuarry.SYNC_CONFIGS)
		{
			SimpleQuarry.LOG.info("Accepted server configs: " + var.get());

			NBTTagCompound data = SimpleQuarry.SYNC_CONFIGS.get();

			bpcOverride = OptionalDouble.of(data.getFloat("BPC"));
			pqrOverride = OptionalInt.of(data.getInteger("QuarryRecipe"));

			NBTTagCompound upgrades = data.getCompoundTag("Upgrades");
			ItemUpgrade.ALL_UPGRADES
					.forEach(u ->
					{
						NBTTagCompound comp = upgrades.getCompoundTag(u.getRegistryName().toString());
						u.quarryUseMultiplierClient = comp.getFloat("Mult");
					});
		}
	}

	public static double getBlockPerCoal()
	{
		return bpcOverride.orElse(BLOCKS_PER_COAL);
	}

	public static int getQuarryRecipe()
	{
		return pqrOverride.orElse(POWERED_QUARRY_RECIPE);
	}
}