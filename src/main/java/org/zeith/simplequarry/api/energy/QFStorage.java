package org.zeith.simplequarry.api.energy;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

public class QFStorage implements IQFConsumer, IQFProducer
{
	public double storedQF = 0.0;
	public double capacity = 1.0;
	
	public QFStorage(double capacity)
	{
		this.capacity = capacity;
	}
	
	/**
	 * Stabilizes QF energy to prevent infinities, NaNs and too little numbers,
	 * also forces in-bound values for fuel.
	 */
	public void fixPower()
	{
		if(Double.isInfinite(this.storedQF) || Double.isNaN(this.storedQF) || this.storedQF <= 1.0E-2)
			this.storedQF = 0F;
		this.storedQF = Math.max(0, Math.min(capacity, this.storedQF));
	}
	
	public QFStorage(double capacity, double QF)
	{
		this(capacity);
		fixPower();
	}
	
	@Override
	public boolean canConnectQF(EnumFacing to)
	{
		return true;
	}
	
	@Override
	public double getStoredQF(EnumFacing to)
	{
		fixPower();
		return this.storedQF;
	}
	
	@Override
	public double getQFCapacity(EnumFacing to)
	{
		return this.capacity;
	}
	
	@Override
	public double produceQF(EnumFacing to, double howMuch, boolean simulate)
	{
		fixPower();
		double extracted = Math.min(howMuch, this.storedQF);
		if(!simulate)
			this.storedQF -= extracted;
		return extracted;
	}
	
	@Override
	public double consumeQF(EnumFacing from, double howMuch, boolean simulate)
	{
		fixPower();
		double accepted = Math.min(this.capacity - this.storedQF, howMuch);
		if(!simulate)
			this.storedQF += accepted;
		return accepted;
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		fixPower();
		nbt.setDouble("QFStored", this.storedQF);
		nbt.setDouble("QFCapacity", this.capacity);
		return nbt;
	}
	
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.storedQF = nbt.getDouble("QFStored");
		this.capacity = nbt.getDouble("QFCapacity");
		fixPower();
	}
	
	public static QFStorage readQFStorage(NBTTagCompound nbt)
	{
		return new QFStorage(nbt.getDouble("QFCapacity"), nbt.getDouble("QFStored"));
	}
}